# Kalender

Kalender ,

This project has been made using Angular 6 & Laravel




INTERFAZ--------------------------
	Cuando accedemos a la Aplicacion Web, podremos observar que en la parte de arriba
	esta el menu, desde aquí podremos acceder a los diferentes modulos.
    En este caso los modulos que estan editados son calendar y auth.

----------------------------Calendars
	Cuando entramos aqui nos apareceran todos los calendarios, si hay muchos apareceran paginados,
    tambien podremos realizar una busqueda de cualquier calendario buscando por el nombre,
    la lista de los calendarios se actualizara con lo que busquemos y la paginacción se actualizará con
    correlación al filtro.
		-------------------Calendario
			Una vez entramos a un calendario se generara el calendario para todo un año desde 
            enero hasta diciembre, cuando pasamos de un mes a otro puede cambiar los dias de
            duracion ya que estan guardados mediante su nombre el numero de dias que tiene.
            En rojo nos aparecerà el dia de hoy.
            Tambien en BD hay un parametro llamado extras, este parametro se pondran las fechas
            en las que no vamos a trabajar, es decir si ponemos un dia, ese dia aparecera en gris
            y no se podra interactuar con él.
            Una vez pulsamos en un dia se pasaran los datos al modal y el modal mostrara los 
            datos de dicho dia y generara los botones dependiendo del horario, por ejemplo, si el
            horario es desde las 8 a las 12 solo se generaran botones entre esas horas con un periodo de 1h.
            Los botones si, hay algun usuario suscrito saldra en rojo, si nos hemos suscrito nosotros
            podremos desuscribirnos, si es de otro usuario lo veremos en rojo pero no podremos
            interactuar con él.
            En caso de que el usuario no este registrado cuando quiera suscribirse sera redireccionado al
            sign in para logearse.
		--------------------------
----------------------------------

-----------------------------LOGIN
SIGN IN
	Desde el sign in podemos loggearnos, introducimos el email, y contraseña
	Si se ha realizado con exito, mostrará un toastr y redireccionara al Home

SIGN IN SOCIAL
	Podremos loggearnos clicando en la red social de google. Podremos loggearnos desde el propio
    sign in o incluso desde el sign up, sin condición de que ya hayamos accedido previamente a traves de 
    google.

SIGN UP
	Desde el sign up nos podremos registrar, siempre se realizará como usuario normal (user)
	Si se ha realizado con exito

LOGOUT
	Cuando pulsamos logout, desde el settings, se destruirá la sesion del usuario.
----------------------------------