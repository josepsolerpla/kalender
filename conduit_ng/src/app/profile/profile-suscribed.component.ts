import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

import { Calendar, CalendarListConfig } from '../core';

@Component({
  selector: 'app-suscribed-page',
  templateUrl: './profile-suscribed.component.html',
})

export class ProfileSuscribedComponent implements OnInit {
  errors: Object = {};
  listConfig: CalendarListConfig = {
    type: 'all',
    filters: {}
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager
  ) {}
  calendarsLoaded = false;

  ngOnInit() {
    this.setListTo('all');
  }

  setListTo(type: string = '', filters: Object = {}) {
    this.listConfig = {type: type, filters: filters};
  }
}
