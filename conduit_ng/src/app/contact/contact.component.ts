import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

import { Contact , ContactService } from '../core';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
  contact: Contact = {} as Contact;
  contactForm: Object = {};
  errors: Object = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private contactService: ContactService,
    private fb: FormBuilder,
    public toastr: ToastrManager
  ) {}

  ngOnInit() {
    // Load info on form or something
  }

  submitForm() {
    this.contactService.send(this.contactForm)
    .subscribe(
      data => {
        this.toastr.successToastr('The Email has been seended', 'Success!');
        this.router.navigateByUrl('/');
      },
      err =>{
            this.toastr.errorToastr('Error has occured,try it later.', 'Oops!');
            console.log(err.errors.message)
            this.errors = err;
      }
    );
  }
}
