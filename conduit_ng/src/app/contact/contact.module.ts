import { NgModule } from '@angular/core';
import {ContactComponent} from './contact.component';
import { SharedModule } from '../shared';
import {ContactRoutingModule} from './contact-routing.module';

import { ToastrModule } from 'ng6-toastr-notifications';

@NgModule({
  imports: [
    SharedModule,
    ContactRoutingModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    ContactComponent
  ]
})
export class ContactModule { }
