import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthComponent } from './auth.component';
import { NoAuthGuard } from './no-auth-guard.service';
import { SharedModule } from '../shared';
import { AuthRoutingModule } from './auth-routing.module';
import { Angular2SocialLoginModule } from "angular2-social-login";

let providers = {
  "google": {
    "clientId": "772032802523-ipdrbvef8fppriifiq4l644qj9fv8dlu.apps.googleusercontent.com"
  }
};

@NgModule({
  imports: [
    SharedModule,
    AuthRoutingModule,
    Angular2SocialLoginModule
  ],
  declarations: [
    AuthComponent
  ],
  providers: [
    NoAuthGuard
  ]
})
export class AuthModule {}

Angular2SocialLoginModule.loadProvidersScripts(providers);