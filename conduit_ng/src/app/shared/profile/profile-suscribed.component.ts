import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

import { Calendar, CalendarListConfig, CalendarService } from '../../core';

@Component({
  selector: 'app-profile-suscribed',
  templateUrl: './profile-suscribed.component.html',
})
export class CalendarPreview {
  errors: Object = {};
  calendarsToLoad: Calendar[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private CalendarService: CalendarService,
    public toastr: ToastrManager
  ) {}

  @Input() calendars: Calendar;
  @Input() limit: number;
  
  @Input() 
  set config(config: CalendarListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: CalendarListConfig;
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];
  filter = null;
  calendarsLoaded = false;

  setPageTo(pageNumber) {
    this.currentPage = pageNumber;
    this.runQuery();
  }

  runQuery() {
    // Create limit and offset filter (if necessary)
    if (!this.filter) this.filter = undefined;
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.calendar = this.filter;
      this.query.filters.offset =  (this.limit * (this.currentPage - 1));
    }

    this.CalendarService.query(this.query)
    .subscribe(data => {
      this.loading = false;
      this.calendarsToLoad = data.calendars;

      this.totalPages = Array.from(new Array(Math.ceil(data.calendarsCount / this.limit)), (val, index) => index + 1);
    });
  }
}
