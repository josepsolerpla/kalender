import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CalendarService, ApiService, clickDayData } from '../../core';

@Component({
  selector: 'app-calendarModal',
  templateUrl: './calendarModal.component.html'
})
export class calendarModal {
  constructor(
    public toastr: ToastrManager,
    private CalendarService: CalendarService
  ){}
  @Input() set data( data: any) {
    this.clickDay = data.clickDay;
    this.calendarData = data.calendarData;
  }

  clickDay : clickDayData;
  calendarData = null;

  ngOnDestroy(){
    this.clickDay = null;
    this.calendarData = null;
  }
}
