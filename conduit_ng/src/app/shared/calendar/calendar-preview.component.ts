import { Component, Input, Output } from '@angular/core';
import { Calendar , CalendarService} from '../../core';
import { asTextData } from '@angular/core/src/view';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-calendar-preview',
  templateUrl: './calendar-preview.component.html',
  styleUrls: ['./calendar-preview.component.css']
})
export class CalendarPreview {
  @Input() calendar: Calendar;
    constructor(
    private CalendarService: CalendarService,
    public toastr: ToastrManager
  ){}
  /** Print errors of configuration*/
  errors = null;
  /** To show the modal*/
  showModal = false;
  /** Calendar configuration for a year.
   *  This could be upgraded if we take this information from the backend.*/
  calendarConfig = [
    {
      month : "Enero",
      days : 31,
      number:1
    },
    {
      month : "Febrero",
      days : 28,
      number:2
    },
    {
      month : "Marzo",
      days : 31,
      number:3
    },
    {
      month : "Abril",
      days : 30,
      number:4
    },
    {
      month : "Mayo",
      days : 31,
      number:5
    },
    {
      month : "Junio",
      days : 30,
      number:6
    },
    {
      month : "Julio",
      days : 31,
      number:7
    },
    {
      month : "Agosto",
      days : 31,
      number:8
    },
    {
      month : "Septiembre",
      days : 30,
      number:9
    },
    {
      month : "Octubre",
      days : 31,
      number:10
    },
    {
      month : "Noviembre",
      days : 30,
      number:11
    },
    {
      month : "Diciembre",
      days : 31,
      number:12
    },
  ];
  /** Assign the current Month configuration*/
  currentYearNumber = new Date().getFullYear();
  OriginalcurrentMonthNumber = new Date().getMonth()+1;
  currentMonthNumber = new Date().getMonth()+1;
  currentMonthObj = this.monthObject();
  currentDayNumber = new Date().getDate()
  /** Value used to configurate which days will not perfome any function on the calendar.*/
  daysToCancel = [];
  daysToCancelObject = []
  modalData = {
    clickDay : {
      day : null,
      month : null,
      year : null
    },
    calendarData : {}
  };

  setShown(bool){
    this.showModal = bool;
  }
  /**Assign the data to the modal */
  clickDay(data){
    this.showModal = true;
    this.modalData = {
      calendarData : this.calendar,
      clickDay : data,
    };
  }
  /** This function return the current month object*/
  monthObject(){
    try{
      if(this.calendarConfig){
        return this.calendarConfig.filter((obj)=>{
          return obj.number == this.currentMonthNumber;
        })[0]
      }else{throw "error ' monthObject ' Not CalendarConfig"}
    }catch(e){
      this.errors = "Error : " + e
      this.toastr.errorToastr(`${this.errors}`, 'Oops!');
    }
  }
  /**
   * This function generate a Array with all days and month based on ' monthObject() '
   */
  generateCurrentMonth(){
    let currentMonth = this.monthObject();
    let currentMonthArray  = []
    for (let i = 0; i < currentMonth.days; i++) {
      currentMonthArray.push({day:i,month:currentMonth.number,year:this.currentYearNumber})
    }
    return currentMonthArray;
  }
  /**
   * Change the Current Month
   * @param n Has to be 1 or -1
   */
  changeMonth(n){
    try{
      if(typeof(n) == "number"){
        this.currentMonthNumber = this.currentMonthNumber + n;
        this.currentMonthObj = this.monthObject();
      }else{throw "error ' changeMonth ' is not a Number"}
    }catch(e){
      this.errors = "Error : " + e
      this.toastr.errorToastr(`${this.errors}`, 'Oops!');
    }
  }
  /**
   * Check dates values to evaluate with the Extras values
   * 
   * @param date current Day to check
   */
  inOnMonth(Date_){
    let res = false;
    try{
      if(typeof(Date_) == "object"){
        this.calendar.extras.map((obj)=>{
          let calendar_date = obj.split("-").map((item)=>{return parseInt(item)})
          if(calendar_date[0] == Date_.day && calendar_date[1] == Date_.month && calendar_date[2] == Date_.year){
            res = true
          }
        })
      }else{throw "error ' inOnMonth ' is not an array"}
    }catch(e){
      this.errors = "Error : " + e
      this.toastr.errorToastr(`${this.errors}`, 'Oops!');
    }
    return res;
  }
  /**
   * APP STATES
   */
  ngOnInit(){
    /** Will assign the daysToCancel deppending on the Extras values.*/
    this.calendar.extras.forEach((item)=>{
        if(parseInt(item.split("-")[1]) == this.currentMonthNumber){
            this.daysToCancel.push(parseInt(item.split("-")[0]));
            this.daysToCancelObject.push({day:parseInt(item.split("-")[0]),month:parseInt(item.split("-")[1]),year:parseInt(item.split("-")[2])})
        }
      })
      if(this.calendar)this.toastr.successToastr(`${this.calendar.name}`, 'Calendar');
      this.CalendarService.activateCalendar(1)
    }
}
