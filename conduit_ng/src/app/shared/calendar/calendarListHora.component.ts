import { Component, Input} from '@angular/core';
import { CalendarService, UserService } from '../../core';
import { element } from 'protractor';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { ObjectUnsubscribedError } from 'rxjs';

@Component({
  selector: 'app-listHora',
  templateUrl: './calendarListHora.component.html'
})
export class listHora {
  timeMorningStart = "00:00";
  timeMorningEnd = "00:00";
  timeEveningStart = "00:00";
  timeEveningEnd = "00:00";
  nullDates = null;
  constructor(
    private CalendarService: CalendarService,
    private UserService : UserService,
    ){}
  currentUserID = this.UserService.getCurrentUser()
  /**
   * Inputs 
   */
  calendarData : any;
  @Input() set calendar(calendar: any){
    this.calendarData = calendar;
  }
  clickDay = null;
  @Input() set clickFun(data: any){
    this.clickDay = data;
  }
  state : any;
  /**
   * Functions
   */
  /**Parse the string to a date */
  parseDate(date){
    return new Date(Date.parse("Aug 28, 1998 "+ date+":00" ));
  }
  /**add a 0 before the number ##Beutify */
  addZeroBefore(n) {
    return (n < 10 ? '0' : '') + n;
  }
  /**
   * APP STATES
   */
  ngOnDestroy(){
    this.state.unsubscribe();
  }
  ngOnChanges(){
    /**This will assign the values ## if not this will deliver a error becouse this values not exist */
    if(this.calendarData && this.calendarData.normalTime){
      this.timeMorningStart = this.calendarData.normalTime.morning.start;
      this.timeMorningEnd = this.calendarData.normalTime.morning.end;
      this.timeEveningStart = this.calendarData.normalTime.evening.start;
      this.timeEveningEnd = this.calendarData.normalTime.evening.end;
    }
    /**Get all subscribed dates to lock or unlock them */
    if(this.calendarData.slug && this.clickDay){
      let date = this.clickDay.year+"-"+this.clickDay.month+"-"+this.clickDay.day
      this.state = this.CalendarService.getSubscribers(this.calendarData.slug,{date:date}).subscribe(
        (data) => {this.nullDates = data} 
      )
    }
  }
}
