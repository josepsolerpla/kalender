import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

import { Calendar, CalendarListConfig, CalendarService } from '../../core';

@Component({
  selector: 'app-calendar-list',
  templateUrl: './listCalendar.component.html',
  styleUrls: ['./calendar.component.css']
})

export class ListCalendarComponent {
  errors: Object = {};
  calendarsToLoad: Calendar[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private CalendarService: CalendarService,
    public toastr: ToastrManager
  ) {}
  @Input() calendars: Calendar;
  @Input() limit: number;
  @Input() 
  set config(config: CalendarListConfig) {
    if (config) {
      this.query = config;
      this.currentPage = 1;
      this.runQuery();
    }
  }

  query: CalendarListConfig;
  loading = false;
  currentPage = 1;
  totalPages: Array<number> = [1];
  filter = null;
  calendarsLoaded = false;

  onKey(event: any) {
    if (event.target.value) {
      this.filter = event.target.value;
      this.runQuery();
    }
  }

  setPageTo(pageNumber) {
    this.currentPage = pageNumber;
    this.runQuery();
  }
  state : any;
  ngOnDestroy(){
    this.state.unsubscribe();
  }

  runQuery() {
    // Create limit and offset filter (if necessary)
    if (!this.filter) this.filter = undefined;
    if (this.limit) {
      this.query.filters.limit = this.limit;
      this.query.filters.calendar = this.filter;
      this.query.filters.offset =  (this.limit * (this.currentPage - 1));
    }

    this.state = this.CalendarService.query(this.query)
    .subscribe(data => {
      this.loading = false;
      this.calendarsToLoad = data.calendars;

      this.totalPages = Array.from(new Array(Math.ceil(data.calendarsCount / this.limit)), (val, index) => index + 1);
    });
  }
}