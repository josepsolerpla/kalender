import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import {  CalendarService, UserService } from '../../core';

@Component({
  selector: 'app-subscribeButton',
  templateUrl: './subscribeButton.component.html'
})
export class subscribeButton {
  constructor(
      private UserService : UserService,
      private CalendarService: CalendarService,
      private router: Router,
  ) {}
  currentUserID = this.UserService.getCurrentUser()
  /**
   * Inputs
   */
  calendarData: any;
  @Input() set calendar( data: any){
    this.calendarData = data;
  }
  dateData: any;
  @Input() set date( data: any){
    this.dateData = data;
  }
  horaData: any;
  @Input() set hora( data: any){
    this.horaData = data;
  }
  nullDates : any;  
  @Input() set dates(data: any){
    this.nullDates = data;
    if(this.nullDates) this.state = this.checkDate(this.horaData)
  }
  /**
   * Functions
   */
  subscribe(){
    this.UserService.isAuth().subscribe(
      (isAuthed)=>{
        if(!isAuthed){
          this.router.navigateByUrl('/login');
        }
        let date = this.dateData.year + "-" + this.dateData.month + "-" + this.dateData.day;
        this.CalendarService.suscribeTo(this.calendarData.slug,{date:date,hora:this.horaData}).subscribe(
          (res)=>{
            this.state = !this.state; 
          }
        )
      },
      )
  }
  checkDate(timeStart){
    if(this.nullDates && this.nullDates.length != 0){
      let back = this.nullDates.filter((element)=>{
        return element.timeStart == timeStart;
      })
      if(back.length >= 1)return true
    }
    return false
  }
  checkBlock(timeStart){
    if(this.nullDates && this.nullDates.length != 0){
      let back = this.nullDates.filter((element)=>{
        return element.timeStart == timeStart;
      }).filter((element)=>{
        return element.id_user == this.currentUserID.id;
      })
      if(back.length >= 1)return true
    }
    return false
  }
  /**
   * APP STATES
   */
  state = false;
  ngOnDestroy(){
    this.calendarData;
    this.dateData;
    this.horaData;
    this.state = false;
  }
}