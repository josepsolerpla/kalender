import { Component, OnInit } from '@angular/core';
import { CalendarService} from '../../core';

import { User, UserService } from '../../core';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  constructor(
    private CalendarService: CalendarService,
    private userService: UserService
  ) {}

  currentUser: User;

  calendarActive = false;

  ngOnInit() {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
  }
}
