import { Component, Input, Output , EventEmitter } from '@angular/core';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-modal-preview',
  templateUrl: './modal-preview.component.html',
  styleUrls: ['./modal-preview.component.css']
})
export class ModalPreview {
  @Input() type: string;
  constructor(
    public toastr: ToastrManager
  ){}

  @Output() setShown = new EventEmitter<boolean>();
  
  @Input() set shown( data : any){
    this.modalShow = data
  };

  @Input() set data( data: any) {
    this.dataSelected = data;
  }

  calendarData: any;
  dataSelected: any;
  modalShow : any;

  closeModalOutside(event){
    event.toElement.childNodes.forEach((res)=>{
      if(res.className == "modal-content"){
        this.setShown.emit(!this.modalShow)
      }
    })
  }


  /* It close modal */
  closeModal(){
    this.setShown.emit(!this.modalShow)
  }
  changeStateModal($event){
    this.modalShow = $event;
  }
}
