import { NgModule } from '@angular/core';
import {ListCalendarComponent} from './listCalendar.component';
import { CalendarViewComponent } from './viewCalendar.component';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared';
import {CalendarRoutingModule} from './calentar-routing.module';
import { CalendarResolver,SubscriptionsResolver } from './calendar-resolver.service';

import { ToastrModule } from 'ng6-toastr-notifications';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    CalendarRoutingModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    ListCalendarComponent,
    CalendarViewComponent
  ],

  providers: [
    CalendarResolver,
  ]
})
export class CalendarModule { }
