import { Injectable, } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Calendar, CalendarService } from '../core';
import { catchError } from 'rxjs/operators/catchError';

@Injectable()
export class CalendarResolver implements Resolve<Calendar> {
  constructor(
    private CalendarService: CalendarService,
    private router: Router,
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.CalendarService.get(route.params['slug'])
    .pipe(catchError((err) => this.router.navigateByUrl('/')));
  }
}

@Injectable()
export class SubscriptionsResolver implements Resolve<any> {
  constructor(
    private CalendarService: CalendarService,
    private router: Router,
  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    return this.CalendarService.getSubscribers(route.params['slug'])
    .pipe(catchError((err) => this.router.navigateByUrl('/')));
  }
}
