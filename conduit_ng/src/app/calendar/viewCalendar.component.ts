import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ToastrManager } from 'ng6-toastr-notifications';

import { Calendar , CalendarService } from '../core';

@Component({
  selector: 'app-viewCalendar-page',
  templateUrl: './viewCalendar.component.html',
  styleUrls: ['./calendar.component.css']
})

export class CalendarViewComponent implements OnInit {
  errors: Object = {};
  calendar: Calendar;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public toastr: ToastrManager
  ) {}
  
  ngOnInit() {
    this.route.data.subscribe(
      (data: { calendar: Calendar }) => {
        this.calendar = data.calendar;
      }
    );
    }
}