import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListCalendarComponent } from './listCalendar.component';
import { CalendarViewComponent } from './viewCalendar.component';
import { CalendarResolver } from './calendar-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: ListCalendarComponent
  },
  {
    path: ':slug',
    component: CalendarViewComponent,
    resolve: {
      calendar: CalendarResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CalendarRoutingModule {}
