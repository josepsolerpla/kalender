import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './api.service';
import { Contact } from '../models';
import { map } from 'rxjs/operators/map';

@Injectable()
export class ContactService {
  constructor (
    private apiService: ApiService
  ) {}

  send(contactForm): Observable<Contact> {
    return this.apiService
    .post('/contact', contactForm)
    .pipe(map(data => {
      return data;
    }));
  }
}
