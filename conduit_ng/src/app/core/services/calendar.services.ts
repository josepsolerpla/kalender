import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiService } from './api.service';
import { Calendar, CalendarListConfig } from '../models';
import { map } from 'rxjs/operators/map';

@Injectable()
export class CalendarService {
  calendarActivated = false;
  constructor (
    private apiService: ApiService
  ) {}

    query(config: CalendarListConfig): Observable<{calendars: Calendar[], calendarsCount: number}> {
      // Convert any filters over to Angular's URLSearchParams
      const params = {};

      Object.keys(config.filters)
      .forEach((key) => {
        params[key] = config.filters[key];
      });

      return this.apiService.get('/calendar',
        new HttpParams({ fromObject: params })
      );
    }
    /**
     * Function to get all Calendars
     * @param  
     */
    get(slug = ""): Observable<Calendar> {
      return this.apiService.get('/calendar/' + slug)
        .pipe(map(data =>data.calendar));
    }

    getSubscribers(slug = "",data = {}){
      return this.apiService
      .post('/calendar/' + slug + '/showSubscribers',data)
        .pipe(map(data =>data.calendars));
    }
    suscribeTo(slug = "", data = {}){
      return this.apiService
      .post('/calendar/' + slug + '/subscribe',data)
        .pipe(map(data =>data.calendar));
    }
    activateCalendar(boolean){
      this.calendarActivated = boolean;
    }
    isCalendar(){
      return this.calendarActivated;
    }
}
