export interface CalendarListConfig {
    type: string;
  
    filters: {
      calendar?: string,
      user?: string,
      subscribed?: string,
      limit?: number,
      offset?: number
    };
  }
  