interface User {
  email: string;
  token: string;
  username: string;
  bio: string;
  image: string;
  id: string;
}

interface Social_User {
  email: string;
  token: string;
  uid?: number;
  username?: string;
  name?: string;
  bio: string;
  image: string;
}

export {User, Social_User};