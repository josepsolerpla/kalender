import { User } from './user.model';

export interface Calendar {
    name: String,
    normalTime: String,
    extras: string[],
    user_id: User;
}