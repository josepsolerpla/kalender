<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\RealWorld\Filters\Filterable;
use App\RealWorld\Subscribing\Subscribing;

class Calendar extends Model
{
    use Subscribing, Filterable;
    /**
     * The attributes that are mass assignable on create.
     *
     * @var array
     */
    protected $fillable = [
        'slug' , 'name', 'normalTime', 'extras'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];
    // ############################ RELATIONS ############################ 
    /**
     * Get the user that owns the calendar.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    // ############################ FUNCTIONS ############################ 
    /**
     * Set the slug with the name.
     * 
     * @var value , value of the name 
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value,'-');
    }
    /**
     * Get the key name for route model binding.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
    /**
     * Load all required relationships with only necessary content.
     * 
     */
    public function scopeLoadRelations($my)
    {
    }
}
