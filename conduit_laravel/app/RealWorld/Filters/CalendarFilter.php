<?php

namespace App\RealWorld\Filters;

use App\User;
use App\Calendar;

class CalendarFilter extends Filter
{
    /**
     * Filter by author username.
     * Get all the calendars by the user with given username.
     *
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function author($username)
    {
        $user = User::whereUsername($username)->first();

        $userId = $user ? $user->id : null;

        return $this->builder->whereUserId($userId);
    }

    /**
     * Filter by suscribed username.
     * Get all the calendars suscribed by the user with given username.
     *
     * @param $username
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function suscribed($username)
    {
        $user = User::whereUsername($username)->first();

        $calendarIds = $user ? $user->suscribed()->pluck('id')->toArray() : [];

        return $this->builder->whereIn('id', $calendarIds);
    }

    /**
     * Filter by string name.
     * Get all the calendars tagged by the given string name.
     *
     * @param $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function calendar($name)
    {
        if ($name != "undefined") {
            $calendarIds = calendar::where('name', 'LIKE', "%$name%")->pluck('id')->toArray();
        } else {
            $calendarIds = calendar::all()->pluck('id')->toArray();
        }

        return $this->builder->whereIn('id', $calendarIds);
    }
}