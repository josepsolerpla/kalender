<?php

namespace App\RealWorld\Subscribing;

use App\User;
use App\Calendar;

trait Subscribing
{
    public function getsubscribedAttribute()
    {
        if (! auth()->check()) {
            return false;
        }

        if (! $this->relationLoaded('subscribers')) {
            $this->load(['subscribers' => function ($query) {
                $query->where('id_user', auth()->id());
            }]);
        }

        $subscribers = $this->getRelation('subscribers');

        if (! empty($subscribers) && $subscribers->contains('id', auth()->id())) {
            return true;
        }

        return false;
    }
    /**
     * Function to add a suscribe row
     * 
     * This will use the current User, need to send Calendar to attach and values
     */
    public function subscribe(Calendar $calendar , $timeStart , $date , $boolean = false )
    {
        if (! $this->isSubscribed($calendar,$timeStart,$date) && $this->id)
        {
            return $this->subscribing()->attach(
                $calendar,[ 
                'validated' => $boolean ,
                'timeStart' => $timeStart , 
                'date' => $date
                ]);
        }
    }
    /**
     * Function to delete a suscribe row
     * 
     * This will use the current User, need to send the Calendar to detach
     */
    public function unsubscribe(Calendar $calendar)
    {
        return $this->subscribing()->detach($calendar);
    }
    /**
     * Both function at the same time
     * 
     * This will do that
     */
    public function subscribeTo(Calendar $calendar , $timeStart , $date , $boolean = false )
    {
        if (! $this->isSubscribed($calendar,$timeStart,$date) && $this->id)
        {
            return $this->subscribing()->attach(
                $calendar,[ 
                'validated' => $boolean ,
                'timeStart' => $timeStart , 
                'date' => $date
                ]);
        }else
        {
            return $this->subscribing()
            ->wherePivot('timeStart', $timeStart)
            ->wherePivot('date', $date)
            ->detach($calendar);
        }
    }
    /**
     * Method to get the table
     * 
     * Users -> Calendar
     */
    public function subscribing()
    {
        return $this->belongsToMany(Calendar::class, 'subscribe', 'id_user', 'id_calendar')
            ->withPivot('validated', 'timeStart','date');
    }
    /**
     * Methd to get table
     * 
     * Calendar -> User
     */
    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'subscribe', 'id_calendar', 'id_user')
            ->withPivot('validated', 'timeStart','date');
    }
    /**
     * Function to match the Calendar with User
     * 
     * Works like a If this is matched with this do X
     */
    public function isSubscribed(Calendar $calendar,$timeStart,$date)
    {
        return !! $this->subscribing()->where('id_calendar', $calendar->id)
        ->where('timeStart', $timeStart)
        ->where('date', $date)
        ->count();
    }
    /**
     * Function to match the User with Calendar
     * 
     * Works like a If this is matched with this do X
     */
    public function isSubscribeBy(User $user)
    {
        return !! $this->subscribers()->where('id_user', $user->id)->count();
    }
}