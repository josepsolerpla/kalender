<?php

namespace App\RealWorld\Transformers;

class CalendarTransformer extends Transformer
{
    protected $resourceName = 'calendar';

    public function transform($data)
    {
        if($data['slug'])
        {
            return [
                //'id'            => $data['id'],
                'name'          => $data['name'],
                'slug'          => $data['slug'],
                'normalTime'    => json_decode($data['normalTime']),
                'extras'        => json_decode($data['extras']),
                'user' => [
                    'id'        => $data['user']['id'],
                    'username'  => $data['user']['username'],
                    'bio'       => $data['user']['bio'],
                    'image'     => $data['user']['image'],
                    'following' => $data['user']['following'],
                ]
            ];
        }elseif($data['pivot'])
        {
            return [
                "id_calendar"=> $data['pivot']['id_calendar'],
                "id_user"=> $data['pivot']['id_user'],
                "validated"=> $data['pivot']['validated'],
                "timeStart"=> $data['pivot']['timeStart'],
                "date"=> $data['pivot']['date']
            ];
        }
    }
}