<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\ContactForm;
use Mail;

class ContactController extends ApiController{

    public function sendEmail(ContactForm $request) {
        $data = array();

        $email = $request;
        $sent = Mail::send('emails.email', $data, function ($message) use($request){
            $message->subject('Contact From App');
            $message->from('josepsolerpla@gmail.com', 'josepsolerpla');
            $message->to($request->email);
        });
        if($sent) dd("something wrong");
        
        return response()->json(['message' => 'Request completed']);
    }
}