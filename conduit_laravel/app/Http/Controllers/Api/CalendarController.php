<?php

namespace App\Http\Controllers\Api;

use App\Calendar;
use App\Http\Requests\Api\showSubscribers;
use App\Http\Requests\Api\subscribe;
use App\RealWorld\Paginate\Paginate;
use App\RealWorld\Filters\CalendarFilter;
use App\RealWorld\Transformers\CalendarTransformer;
use Request;
use DB;

class CalendarController extends ApiController{
    /**
     * CalendarController constructor.
     *
     * @param CalendarTransformer $transformer
     */
    public function __construct(CalendarTransformer $transformer)
    {
        $this->transformer = $transformer;

        $this->middleware('auth.api')->except('show','showSuscribers','index');
        $this->middleware('auth.api:optional')->only('show');
    }
    /**
     * Get all the calendars paginated.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CalendarFilter $filter)
    {
        //print_r($request);
        $calendars = new Paginate(Calendar::loadRelations()->filter($filter));
        
        return $this->respondWithPagination($calendars);
    }
    /**
     * Get the calendars given by its slug.
     *
     * @param Calendar $calendar
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Calendar $calendar)
    {
        return $this->respondWithTransformer($calendar);
    }

    public function showSubscribers(Calendar $calendar,showSubscribers $request)
    {   
        $calendar = $calendar->subscribers(); // #subscribers its a function from Subscribing Trait invoqued on Calendar Model

        if($request->input('validated'))
            $calendar = $calendar->where('validated','=',true);
        else
            $calendar = $calendar->where('validated','=',false);
        if($request->input('timeStart'))
            $calendar = $calendar->where('timeStart',$request->input('timeStart'));
        if($request->input('date'))
            $calendar = $calendar->where('date','=',$request->input('date'));

        return $this->respondWithTransformer($calendar->get());
    }
    /**
     * Subscribe to Calendar
     */
    public function subscribe(Calendar $calendar, subscribe $request){
        $authenticatedUser = auth()->user();

        $hora = $request->input('hora');
        $date = $request->input('date');
        
        $authenticatedUser->subscribeTo(
            $calendar,
            $hora,
            $date,
            false
        );

        return $this->respondWithTransformer($calendar);
    }
}
