<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\User;
use App\Http\Requests\Api\LoginUser;
use App\Http\Requests\Api\RegisterUser;
use App\RealWorld\Transformers\UserTransformer;
use Illuminate\Http\Request;

class AuthController extends ApiController
{
    /**
     * AuthController constructor.
     *
     * @param UserTransformer $transformer
     */
    public function __construct(UserTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * Login user and return the user if successful.
     *
     * @param LoginUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginUser $request)
    //public function login(Request $request)
    {
        $credentials = $request->only('user.email', 'user.password');
        $credentials = $credentials['user'];

        if (! Auth::once($credentials)) {
            return $this->respondFailedLogin();
        }
        
        //return response()->json($request->get('user'), 200, []);
        //return response()->json($request->input('user.email'), 200, []);
        //return response()->json(auth()->user(), 200, []);

        return $this->respondWithTransformer(auth()->user());
    }

    /**
     * Register a new user and return the user if successful.
     *
     * @param RegisterUser $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterUser $request)
    //public function register(Request $request)
    {
        $user = User::create([
            'username' => $request->input('user.username'),
            'email' => $request->input('user.email'),
            'password' => $request->input('user.password'),
        ]);
        
        //return response()->json($request->get('user'), 200, []);
        //return response()->json($request->input('user.username'), 200, []);
        //return response()->json($user, 200, []);
        
        return $this->respondWithTransformer($user);
    }

    /**
     * Register a new social user and return the user if successful.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function Social(Request $request)
    {
        try {
            $isthere = User::where('email', $request->input('user.email'), $request->input('user.password'))->first();
            if ($isthere) {
                return $this->respondWithTransformer($isthere);
            }else {
                $user = User::create([
                    'username' => $request->input('user.username'),
                    'email' => $request->input('user.email'),
                    'password' => $request->input('user.password'),
                    'image' => $request->input('user.image')
                ]);
                
                return $this->respondWithTransformer($user);
            }
        }catch (Exception $e) {
            return $this->respondError($e, "500");
        }
    }
}
