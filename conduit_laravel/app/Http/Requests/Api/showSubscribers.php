<?php

namespace App\Http\Requests\Api;

class showSubscribers extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'validated' => '',
            'timeStart' => '',
            'date' => '',
        ];
    }
}
