<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {
    /**
     * Calendar
     */
    Route::get('/calendar', 'CalendarController@index');
    Route::get('/calendar/{calendar}', 'CalendarController@show');
    Route::post('/calendar/{calendar}/showSubscribers','CalendarController@showSubscribers');
    Route::post('/calendar/{calendar}/subscribe', 'CalendarController@subscribe');
    /**
     * User
     */
    Route::post('users/login', 'AuthController@login');
    Route::post('users/social', 'AuthController@social');
    Route::post('users', 'AuthController@register');

    Route::get('user', 'UserController@index');
    Route::match(['put', 'patch'], 'user', 'UserController@update');
    /**
     * Profile
     */
    Route::get('profiles/{user}', 'ProfileController@show');
    Route::post('profiles/{user}/follow', 'ProfileController@follow');
    Route::delete('profiles/{user}/follow', 'ProfileController@unFollow');
    /**
     * Articles
     */
    Route::get('articles/feed', 'FeedController@index');
    Route::post('articles/{article}/favorite', 'FavoriteController@add');
    Route::delete('articles/{article}/favorite', 'FavoriteController@remove');

    Route::resource('articles', 'ArticleController', [
        'except' => [
            'create', 'edit'
        ]
    ]);

    Route::resource('articles/{article}/comments', 'CommentController', [
        'only' => [
            'index', 'store', 'destroy'
        ]
    ]);
    /**
     * Tags
     */
    Route::get('tags', 'TagController@index');
    /**
     * Contact
     */
    Route::post('contact', 'ContactController@sendEmail');
});